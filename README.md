# [安卓知识点目录](https://gitee.com/yaosycode/android-catalog)
[记录安卓开发涉及的知识点](https://naotu.baidu.com/file/9bff666cf0838668257e4878ee95c0f9)

## [系统架构](1、语法和界面/1.系统架构.md)
## [环境搭建](1、语法和界面/环境搭建.md)
## [Java和Kotlin语法](1、语法和界面/Java和Kotlin语法.md)
## [四大组件](1、语法和界面/四大组件.md)
## [UI组件](1、语法和界面/UI组件.md)

## [动画](2、事件和交互/动画.md)
## [数据处理](2、事件和交互/数据处理.md)
## [事件交互](2、事件和交互/事件交互.md)
## [自定义组件](2、事件和交互/自定义组件.md)

## [Jetpack](3、框架体系架构/Jetpack.md)
## [架构](3、框架体系架构/架构.md)
## [跨平台](3、框架体系架构/跨平台.md)

## [NDK](4、开源框架/NDK.md)
## [FFmpeg](4、开源框架/FFmpeg.md)
## [插桩技术](4、开源框架/插桩技术.md)
## [三方库](4、开源框架/三方库.md)
## [性能优化](4、开源框架/性能优化.md)
## [源码分析](4、开源框架/源码分析.md)

## [资源文件](5、打包与发布/资源文件.md)
## [适配](5、打包与发布/适配.md)
### [构建与打包](5、打包与发布/构建与打包.md)

## 知识点
1. Application
2. Context
3. Fragment

## 高级技术
NDK
JNI
AES插桩

## 参考文档
1. Android官网 https://developer.android.google.cn/?hl=zh-cn
2. Flutter官网 https://flutter.cn/docs
3. Android源码（7以上） http://aospxref.com/
4. Android源码（9以下） http://androidxref.com/
   https://www.androidos.net.cn/sourcecode